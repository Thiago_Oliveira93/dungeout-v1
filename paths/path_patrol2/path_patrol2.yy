{
    "id": "7d7db461-947f-405c-9e59-e904802e2a4c",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_patrol2",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "4a5a179e-2890-43a6-8097-3fc15256a0e4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 608,
            "y": 1632,
            "speed": 100
        },
        {
            "id": "6f68f94d-97dd-4c96-af0f-0a49374d8c20",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 1632,
            "speed": 100
        },
        {
            "id": "636387e4-bc76-49fe-ba55-a9cfe5f48402",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 960,
            "y": 1152,
            "speed": 100
        },
        {
            "id": "f28e1a2a-8c85-47e6-8952-1cfa956322ae",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 640,
            "y": 1152,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}