{
    "id": "69adc7bb-2074-4a1a-9c42-3f6b6ea7e547",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path_patrol1",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "6f9b8fec-c56f-4c3b-9b5a-abd3d8c4d01e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 1856,
            "speed": 100
        },
        {
            "id": "59709442-4a88-478e-a7ae-a2b768b3a2b6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 1472,
            "speed": 100
        },
        {
            "id": "bf97e256-c0ac-4c56-9d7e-a3e4f181f09b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 1472,
            "speed": 100
        },
        {
            "id": "fbb4393c-82ea-49f9-885a-a159ed216768",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 1888,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}