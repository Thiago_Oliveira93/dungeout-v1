{
    "id": "5dd32291-5887-4de0-add7-f9a3cc8a9e9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_spike",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c2b5cc1-0214-46cc-928b-f1372ad687da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dd32291-5887-4de0-add7-f9a3cc8a9e9f",
            "compositeImage": {
                "id": "c0cfadcd-f7ad-4b88-9d4d-bf965a47570d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c2b5cc1-0214-46cc-928b-f1372ad687da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1581b6c-2589-49da-ab9e-fcb094589c7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c2b5cc1-0214-46cc-928b-f1372ad687da",
                    "LayerId": "9bc54eb1-4f93-47d9-acac-761a97bcbfb2"
                }
            ]
        },
        {
            "id": "55cfa6c8-0be9-4d27-9677-225a12068286",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5dd32291-5887-4de0-add7-f9a3cc8a9e9f",
            "compositeImage": {
                "id": "81903082-917c-4501-a1af-eedfc93dc006",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55cfa6c8-0be9-4d27-9677-225a12068286",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89d6b111-1a1b-4cb2-95e6-dbfcfda695a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55cfa6c8-0be9-4d27-9677-225a12068286",
                    "LayerId": "9bc54eb1-4f93-47d9-acac-761a97bcbfb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9bc54eb1-4f93-47d9-acac-761a97bcbfb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5dd32291-5887-4de0-add7-f9a3cc8a9e9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}