{
    "id": "1d1ab005-60f3-497b-aa5f-a45980e494ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_eyes",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9069c086-6f03-4600-a584-553eb9a6d032",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d1ab005-60f3-497b-aa5f-a45980e494ab",
            "compositeImage": {
                "id": "27d326d1-92b4-4a11-a82f-2997ba7eaa28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9069c086-6f03-4600-a584-553eb9a6d032",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b3992c9-9c1a-420c-ad1f-6c4c18c2f8bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9069c086-6f03-4600-a584-553eb9a6d032",
                    "LayerId": "f2702c13-0bc6-485d-9bc9-8e194b9b993f"
                }
            ]
        },
        {
            "id": "d181ee74-c6f5-415b-b6b3-bae0454d870a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d1ab005-60f3-497b-aa5f-a45980e494ab",
            "compositeImage": {
                "id": "eac963e1-3e91-4e08-a60d-5acda51a3f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d181ee74-c6f5-415b-b6b3-bae0454d870a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59fe9895-3f89-4ffb-b9f1-6976a51154e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d181ee74-c6f5-415b-b6b3-bae0454d870a",
                    "LayerId": "f2702c13-0bc6-485d-9bc9-8e194b9b993f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "f2702c13-0bc6-485d-9bc9-8e194b9b993f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d1ab005-60f3-497b-aa5f-a45980e494ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 3
}