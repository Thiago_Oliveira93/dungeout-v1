{
    "id": "30e3f141-5e13-4abc-8b70-fac8faa2b026",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d5c7e31-e59e-4dcd-8a8e-b4edb6eef760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30e3f141-5e13-4abc-8b70-fac8faa2b026",
            "compositeImage": {
                "id": "0747e668-3d56-42c3-bc39-57c5be44f7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d5c7e31-e59e-4dcd-8a8e-b4edb6eef760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8747af59-07d1-4b94-ae35-c2d01b4778e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d5c7e31-e59e-4dcd-8a8e-b4edb6eef760",
                    "LayerId": "0ffcd015-f993-44a1-a6e7-0b3e696df515"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "0ffcd015-f993-44a1-a6e7-0b3e696df515",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30e3f141-5e13-4abc-8b70-fac8faa2b026",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}