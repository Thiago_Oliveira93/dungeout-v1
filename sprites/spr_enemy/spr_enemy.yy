{
    "id": "9bb6e361-1758-49bd-b141-e9c877aba4a4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a510aa42-bc30-41ec-b7a8-d778612d6468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9bb6e361-1758-49bd-b141-e9c877aba4a4",
            "compositeImage": {
                "id": "8fcacf76-f816-4e7a-929d-21c70b5e9a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a510aa42-bc30-41ec-b7a8-d778612d6468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65917e5-87ee-4ec7-b366-7606305756f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a510aa42-bc30-41ec-b7a8-d778612d6468",
                    "LayerId": "85ef934c-d41f-4276-84c8-1f844620a8eb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "85ef934c-d41f-4276-84c8-1f844620a8eb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9bb6e361-1758-49bd-b141-e9c877aba4a4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}