{
    "id": "d2f89b2a-cac7-4064-a71f-b55b7c4a9aa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerfield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 178,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04b4c6a5-52d9-418f-82ed-1a06661d430a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2f89b2a-cac7-4064-a71f-b55b7c4a9aa8",
            "compositeImage": {
                "id": "c91198b5-b940-4484-8b5c-4400ea8e7bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04b4c6a5-52d9-418f-82ed-1a06661d430a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd4cd5a3-5aed-44cd-9ef3-e18806b54d6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04b4c6a5-52d9-418f-82ed-1a06661d430a",
                    "LayerId": "95f26efb-2393-4385-a21e-1b9d8de6e035"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "95f26efb-2393-4385-a21e-1b9d8de6e035",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2f89b2a-cac7-4064-a71f-b55b7c4a9aa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 180,
    "xorig": 90,
    "yorig": 90
}