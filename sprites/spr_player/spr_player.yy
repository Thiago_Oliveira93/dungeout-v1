{
    "id": "ad00440d-0d96-484b-ba12-6ad5115117cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3aed2d59-8d16-4842-b45c-38d92edd9def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad00440d-0d96-484b-ba12-6ad5115117cf",
            "compositeImage": {
                "id": "335e6a4f-876a-4d17-b543-e016613d0fcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3aed2d59-8d16-4842-b45c-38d92edd9def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4c30c92-ac96-4d5b-930f-48c1c51e7d94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3aed2d59-8d16-4842-b45c-38d92edd9def",
                    "LayerId": "1252f976-e8d6-407f-a4f2-3cac5c3704a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1252f976-e8d6-407f-a4f2-3cac5c3704a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad00440d-0d96-484b-ba12-6ad5115117cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}