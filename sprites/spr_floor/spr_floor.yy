{
    "id": "b6549ae5-3ccd-4120-b917-83e0f7277a3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a4ba39b-5681-47cc-bb93-11eca69303e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6549ae5-3ccd-4120-b917-83e0f7277a3d",
            "compositeImage": {
                "id": "7841e064-452e-494a-8991-6b3bfe77fd34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a4ba39b-5681-47cc-bb93-11eca69303e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d8f91dd-7a20-4015-94ff-7e534be84710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a4ba39b-5681-47cc-bb93-11eca69303e6",
                    "LayerId": "9dd4ca64-2cf6-4b89-b65b-0656fe663373"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "9dd4ca64-2cf6-4b89-b65b-0656fe663373",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6549ae5-3ccd-4120-b917-83e0f7277a3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}