{
    "id": "48ac68df-a1ea-40e4-a7ea-6b7f7b843a7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 111,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d89c000-16d0-4b73-8fb0-afd76d64791f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48ac68df-a1ea-40e4-a7ea-6b7f7b843a7b",
            "compositeImage": {
                "id": "7ef22f53-769c-42ec-80d3-1c75a77d215f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d89c000-16d0-4b73-8fb0-afd76d64791f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9142110e-6c66-407a-9cc5-e3be8cd2240e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d89c000-16d0-4b73-8fb0-afd76d64791f",
                    "LayerId": "d678d844-daee-4637-a6d8-2ba12ba5fd5b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d678d844-daee-4637-a6d8-2ba12ba5fd5b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48ac68df-a1ea-40e4-a7ea-6b7f7b843a7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 64
}