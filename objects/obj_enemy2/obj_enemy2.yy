{
    "id": "8e971cff-7790-4936-bde8-39906ac335c4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy2",
    "eventList": [
        {
            "id": "7a97bffc-41b6-4e60-bf8b-f014b79cbf5a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e971cff-7790-4936-bde8-39906ac335c4"
        },
        {
            "id": "cf2543bc-68e4-4e15-b1a5-b0be0d586ee0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e971cff-7790-4936-bde8-39906ac335c4"
        },
        {
            "id": "d5421ad2-fbb5-4a55-aec2-061238c16963",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8e971cff-7790-4936-bde8-39906ac335c4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9bb6e361-1758-49bd-b141-e9c877aba4a4",
    "visible": true
}