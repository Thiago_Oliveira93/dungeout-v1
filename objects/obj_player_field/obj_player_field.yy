{
    "id": "3f6d8ef1-e3c3-4941-bc29-32667204fe51",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_field",
    "eventList": [
        {
            "id": "682adb09-7b4e-4507-a47a-9faf4ee3f49b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3f6d8ef1-e3c3-4941-bc29-32667204fe51"
        },
        {
            "id": "d98a69b5-7714-458a-840f-283a0da637ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3f6d8ef1-e3c3-4941-bc29-32667204fe51"
        },
        {
            "id": "d5acac73-eadc-462d-af1d-ea8354be15f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3f6d8ef1-e3c3-4941-bc29-32667204fe51"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2f89b2a-cac7-4064-a71f-b55b7c4a9aa8",
    "visible": true
}