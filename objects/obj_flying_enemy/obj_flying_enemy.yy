{
    "id": "30bf350c-1629-4b0b-bbe5-bdc1859eeb3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_flying_enemy",
    "eventList": [
        {
            "id": "7570924d-93af-48d4-8884-fa82c02250bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "30bf350c-1629-4b0b-bbe5-bdc1859eeb3c"
        },
        {
            "id": "0b4404cd-563f-4aa4-8728-2453633b5dce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "30bf350c-1629-4b0b-bbe5-bdc1859eeb3c"
        },
        {
            "id": "264c19fc-15d4-4a23-a310-3c0a753d87ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "30bf350c-1629-4b0b-bbe5-bdc1859eeb3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9bb6e361-1758-49bd-b141-e9c877aba4a4",
    "visible": true
}