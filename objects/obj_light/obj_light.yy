{
    "id": "5cc8ef85-0e7d-4023-baf8-ffdbdca4c36c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_light",
    "eventList": [
        {
            "id": "2ecad1a7-e6e8-4d45-985e-ed1776ae08d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cc8ef85-0e7d-4023-baf8-ffdbdca4c36c"
        },
        {
            "id": "aa93a158-d841-482f-9b58-4a36fe9a6b03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5cc8ef85-0e7d-4023-baf8-ffdbdca4c36c"
        },
        {
            "id": "d738a8d4-1318-454c-a8d4-ae0376f80ef9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5cc8ef85-0e7d-4023-baf8-ffdbdca4c36c"
        },
        {
            "id": "7efe0083-e9dd-4756-a01a-391dc6cba16f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "5cc8ef85-0e7d-4023-baf8-ffdbdca4c36c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}