/// @description Insert description here
///
if (surface_exists(surf))
{
	surface_set_target(surf)
	
	///draw the dark overlay
	draw_set_color(c_black);
	//draw_set_alpha(0.8);
	draw_rectangle(0,0,room_width,room_height,false);
	
	///Set circles
	gpu_set_blendmode(bm_subtract);///define o tipo de blend mode
	
	
	
	draw_set_color(c_black);
	draw_set_alpha(0.2);
	
	///draw circles
	
	with (obj_player)
	{
		draw_circle((x+random_range(-2,2)),(y+random_range(-2,2)),radius1+20+random_range(-1,1),false)
	}
	
	draw_set_color($448CCB);
	draw_set_alpha(0.2)
	with (obj_player)
	{
		draw_circle((x+random_range(-2,2)),(y+random_range(-2,2)),obj_player.radius1+random_range(-1,1),false)
	}
	
	
	
	with (obj_enemy)
	{
		//if obj_enemy.light
		//{
			draw_circle((x+random_range(-1,1)),(y+random_range(-1,1)),obj_enemy.radius+random_range(-1,1),false)
		//}
	}
	
	with (obj_flying_enemy)
	{
		//if obj_enemy.light
		//{
			draw_circle((x+random_range(-1,1)),(y+random_range(-1,1)),obj_flying_enemy.radius+random_range(-1,1),false)
		//}
	}
	
	with (obj_grave)
	{
		//draw_set_color(c_orange);
		draw_set_alpha(obj_grave.alpha)
		draw_circle((x+random_range(-2,2)),(y+random_range(-2,2)),300+random_range(-1,1),false)
	}
	
	
	///Reset all of the set draws
	gpu_set_blendmode(bm_normal);
	draw_set_alpha(1)
	surface_reset_target()
	
	
	
	
	
}
else
{
	///if dont create, create!
	surf = surface_create(room_width,room_height);
	surface_set_target(surf);
	draw_clear_alpha(c_black, 0);
	surface_reset_target();
}