{
    "id": "992e288b-fdca-4b82-9231-dcea94f554a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "6bba40eb-912b-4962-b851-265b94e9d42b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "992e288b-fdca-4b82-9231-dcea94f554a4"
        },
        {
            "id": "8f2b9a31-ef8d-4742-987c-89e37c1ff944",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "992e288b-fdca-4b82-9231-dcea94f554a4"
        },
        {
            "id": "6438e9f0-cbb4-4e87-ae11-06c064b6cb69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "992e288b-fdca-4b82-9231-dcea94f554a4"
        },
        {
            "id": "4a76b746-1767-4c3a-aa8f-9865028d93d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "992e288b-fdca-4b82-9231-dcea94f554a4"
        },
        {
            "id": "dc882903-cda2-41c3-9b3a-2b94072e11ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d2fcaf29-9b59-4c1e-ac84-d0bd5c0b0953",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "992e288b-fdca-4b82-9231-dcea94f554a4"
        },
        {
            "id": "499d980b-8ce0-4a17-b92f-20a0772615cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "30bf350c-1629-4b0b-bbe5-bdc1859eeb3c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "992e288b-fdca-4b82-9231-dcea94f554a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ad00440d-0d96-484b-ba12-6ad5115117cf",
    "visible": true
}