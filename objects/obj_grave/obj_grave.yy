{
    "id": "9c4e162f-8ae1-4c35-ad09-b08da9f995ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grave",
    "eventList": [
        {
            "id": "20527c73-9fce-4e89-ab54-3cf693d21b54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9c4e162f-8ae1-4c35-ad09-b08da9f995ca"
        },
        {
            "id": "26979cb4-bed4-4507-bc0f-91e317c54214",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9c4e162f-8ae1-4c35-ad09-b08da9f995ca"
        },
        {
            "id": "67728331-a8c4-4239-99d3-96a12e283e9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c4e162f-8ae1-4c35-ad09-b08da9f995ca"
        },
        {
            "id": "155ba0e7-02a3-4a53-91c2-9a138d3c7646",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9c4e162f-8ae1-4c35-ad09-b08da9f995ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30e3f141-5e13-4abc-8b70-fac8faa2b026",
    "visible": true
}