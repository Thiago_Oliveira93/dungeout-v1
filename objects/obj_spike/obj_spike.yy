{
    "id": "d2fcaf29-9b59-4c1e-ac84-d0bd5c0b0953",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spike",
    "eventList": [
        {
            "id": "a980fd84-664d-4a28-ba82-08e96bde0649",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d2fcaf29-9b59-4c1e-ac84-d0bd5c0b0953"
        },
        {
            "id": "5a8c7c3e-843e-4e06-979e-94f1512e2f3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d2fcaf29-9b59-4c1e-ac84-d0bd5c0b0953"
        },
        {
            "id": "ef7841cd-471c-40d5-864c-5160a714d312",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d2fcaf29-9b59-4c1e-ac84-d0bd5c0b0953"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5dd32291-5887-4de0-add7-f9a3cc8a9e9f",
    "visible": true
}