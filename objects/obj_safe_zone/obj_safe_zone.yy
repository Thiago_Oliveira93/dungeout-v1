{
    "id": "7f39ae7e-28ba-4965-aa61-c3fc2e656cea",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_safe_zone",
    "eventList": [
        {
            "id": "f4ff2c82-561a-424e-8193-9c161e21d89e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f39ae7e-28ba-4965-aa61-c3fc2e656cea"
        },
        {
            "id": "6716d116-c2f9-4141-9ea2-367809aed39f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f39ae7e-28ba-4965-aa61-c3fc2e656cea"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d2f89b2a-cac7-4064-a71f-b55b7c4a9aa8",
    "visible": false
}