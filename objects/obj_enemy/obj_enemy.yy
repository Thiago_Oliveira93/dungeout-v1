{
    "id": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "25326915-7be5-4903-9bec-2e6259bb4afb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        },
        {
            "id": "e3ede879-8c0c-4bcd-8714-89992bc9c64f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        },
        {
            "id": "41c5ed0e-b749-41b3-8da4-8bbd3aa19b11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        },
        {
            "id": "52acbff0-d87f-4c9c-8a98-63f1cd7e8776",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        },
        {
            "id": "569e1a78-2590-4a6f-89d7-435859ebe32f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f39ae7e-28ba-4965-aa61-c3fc2e656cea",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        },
        {
            "id": "6f0a628e-a6cc-41c5-8048-c9e278c5fcdc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "992e288b-fdca-4b82-9231-dcea94f554a4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        },
        {
            "id": "bb03c65a-19e5-4f81-892c-2a7e62c2e2d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        },
        {
            "id": "b41bffc0-27b1-46af-80dc-bfd0d1e6952f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        },
        {
            "id": "dc45f0b4-a3e7-4bc1-b5be-dabe6d267f74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "ff7bf1ef-09b3-47f8-a5ae-e093f5b8b77e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "e78297c0-1b38-4024-b6a1-9fbf3532d37a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "variable_name",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "9bb6e361-1758-49bd-b141-e9c877aba4a4",
    "visible": true
}