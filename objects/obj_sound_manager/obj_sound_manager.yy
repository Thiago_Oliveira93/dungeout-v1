{
    "id": "0a561f23-4bd4-4995-8db1-92d6e9895994",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sound_manager",
    "eventList": [
        {
            "id": "f6e6e139-8c27-4e3f-99e2-cd4f71b7609b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0a561f23-4bd4-4995-8db1-92d6e9895994"
        },
        {
            "id": "63b7b946-bf78-4fcc-a54d-4d39a69a3c80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0a561f23-4bd4-4995-8db1-92d6e9895994"
        },
        {
            "id": "8dd2a0b0-b785-40c1-9817-8813b93f7ae1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "0a561f23-4bd4-4995-8db1-92d6e9895994"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}