///@arg hspeed
///@arg vspeed

var xspeed = argument0
var yspeed = argument1

if !place_meeting(x+xspeed, y, obj_wall)
{
	x+=argument0
}

if !place_meeting(x,y+yspeed,obj_wall)
{
	y+=argument1
}

